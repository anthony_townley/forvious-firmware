/********************************************
* File: Forvious_Mixer
* Author(s):  Anthony Townley, Steven Thai, Gene Crocetti, Mehran Hossaini
* UMBC Capstone 2015/2016
* Description: This file contains all the necessary code to run the Teensy 3.2
*   to control the 4VS module
***********************************************/

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <Bounce.h>
#include <math.h>
#include <Vector.h>

const float ANALOG_MAX = 1023.0;
const float MAX_FREQUENCY = 44100.0;

//Add File for reading file tracks
File root;
//Add Declaration for parsing files to add to vector of tracks...
void CreateTracks(File dir);


//The following code segment was generated using the online utility provided by Teensy...
// GUItool: begin automatically generated code
AudioPlaySdWav           playSdWav1;     //xy=80,29
AudioPlaySdWav           playSdWav2;     //xy=100,121
AudioPlaySdWav           playSdWav3;     //xy=105,214
AudioPlaySdWav           playSdWav4;     //xy=110,301
AudioEffectBitcrusher    bitcrusher1;    //xy=285,22
AudioEffectBitcrusher    bitcrusher2;    //xy=314,128
AudioEffectBitcrusher    bitcrusher3;    //xy=319,211
AudioEffectBitcrusher    bitcrusher4;    //xy=321,300
AudioMixer4              mixer3;         //xy=533,35
AudioMixer4              mixer2;         //xy=533,126
AudioMixer4              mixer1;         //xy=538,209
AudioMixer4              mixer4;         //xy=541,298
AudioOutputI2SQuad       i2s_quad1;      //xy=786,181
AudioConnection          patchCord1(playSdWav1, 0, bitcrusher1, 0);
AudioConnection          patchCord2(playSdWav3, 1, bitcrusher2, 0);
AudioConnection          patchCord3(playSdWav2, 0, bitcrusher3, 0);
AudioConnection          patchCord4(playSdWav4, 1, bitcrusher4, 0);
AudioConnection          patchCord5(bitcrusher1, 0, mixer1, 0);
AudioConnection          patchCord6(bitcrusher2, 0, mixer3, 0);
AudioConnection          patchCord7(bitcrusher3, 0, mixer2, 0);
AudioConnection          patchCord8(bitcrusher4, 0, mixer4, 0);
AudioConnection          patchCord9(mixer3, 0, i2s_quad1, 2);
AudioConnection          patchCord10(mixer2, 0, i2s_quad1, 1);
AudioConnection          patchCord11(mixer1, 0, i2s_quad1, 0);
AudioConnection          patchCord12(mixer4, 0, i2s_quad1, 3);
AudioControlSGTL5000     sgtl5000_2;     //xy=377,442
AudioControlSGTL5000     sgtl5000_1;     //xy=595,441
// GUItool: end automatically generated code

//Button Declarations using the bounce library provided by Teensy to
// debounce any buttons...
Bounce gate1 = Bounce(0, 15); //Channel 1 Gate
Bounce gate2 = Bounce(1, 15); //Channel 2 Gate
Bounce gate3 = Bounce(2, 15); //Channel 3 Gate
Bounce gate4 = Bounce(3, 15); //Channel 4 Gate
//Bounce encoder0 = Bounce(4, 15); //bit 0 of channel selection encoder
//Bounce encoder1 = Bounce(5, 15); //bit 1 of channel selection encoder
//Bounce encoder2 = Bounce(8, 15); //bit 2 of channel selection encoder


/*Pin allocations*********************************************************** 
 * 
Pin0  - Channel 1 Gate Input
Pin1  - Channel 2 Gate Input
Pin2  - Channel 3 Gate Input
Pin3  - Channel 4 Gate Input
Pin4  - Channel 1 LED Output
Pin5  - Channel 2 LED Output
Pin6 -Audio Shield
Pin7 -Audio Shield
Pin8 - Channel 3 LED Output
Pin9  Audio Shield
Pin10 Audio Shield
Pin11 Audio Shield
Pin12 Audio Shield
Pin13 Audio Shield
Pin14 Audio Shield
Pin15 Audio Shield Quad...
PinA2 -Analog Channel 1 P1 Input
PinA3 -Analog Channel 1 P2 Input
Pin18 Audio Shield
Pin19 Audio Shield
PinA6 -Analog Channel 1 CV1 Input
PinA7 -Analog Channel 1 CV2 Input
Pin22 Audio shield
Pin23 Audio shield
Pin24 - Channel 4 LED Output
Pin25 - LED_S2 Output
PinA10 - Analog Channel 2 P1 Input
PinA11 - Analog Channel 2 P2 Input
PinA12 - Analog Channel 2 CV1 Input
PinA13 - Analog Channel 2 CV2 Input
PinA14 - Analog Channel 3 P1 Input
PinA15 - Analog Channel 3 P2 Input
PinA16 - Analog Channel 3 CV1 Input
PinA17 - Analog Channel 3 CV2 Input
PinA18 - Analog Channel 4 P1 Input
PinA19 Audio Shield Quad Connect
PinA20 - Analog Channel 4 P2 Input
Pin32 - LED_Enable Output
Pin33 - Unused 
************************/


// Use these with the audio adaptor boards
#define SDCARD_CS_PIN    10
#define SDCARD_MOSI_PIN  7
#define SDCARD_SCK_PIN   14

//This vector of strings will hold all the .wav file names for playing...
Vector<String> tracks;

int num; //variable to keep position of track in array of songs...


void setup() {
  //Buttons are connected to external resistors so will be INPUTs
  pinMode(0, INPUT);
  pinMode(1, INPUT);
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(24, OUTPUT);
//  pinMode(32, OUTPUT);
  
  Serial.begin(9600); //For debugging...
  AudioMemory(10);// Determines how much memory we need for Audio... 
  
  /*The next section is setting the codecs on the two audio shields 
   * to work together for quad channel output... */
  sgtl5000_1.setAddress(LOW);
  sgtl5000_1.enable();
  sgtl5000_1.volume(0.5);

  sgtl5000_2.setAddress(HIGH);
  sgtl5000_2.enable();
  sgtl5000_2.volume(0.5);
  
  //Specify the MOSI and SCK pin for the Audio shield...
  SPI.setMOSI(SDCARD_MOSI_PIN);
  SPI.setSCK(SDCARD_SCK_PIN);
  if (!(SD.begin(SDCARD_CS_PIN))) {
    // stop here, but print a message repetitively
    while (1) {
      Serial.println("Unable to access the SD card");
      delay(500);
    } 
  }
  //Begin by creating a file descriptor to read the SD card... 
  root = SD.open("/");
  
  int sz = tracks.size();  // for debugging...

  Serial.print("Size of tracks is: "); // for debugging...
  Serial.println(sz); // "  "
  
  //Call the function to read tracks and save to a Vector...
  CreateTracks(root);

//For debugging... 
  for(int i = 0; i < tracks.size(); i++)
  {
    //Print out each track name to the serial monitor
    Serial.print("Track ");
    Serial.print( i);
    Serial.print(": ");
    Serial.println(tracks[i]);
  }

  Serial.print("Size of tracks is: ");
  sz = tracks.size();
  Serial.println(sz);

  /*Begin by setting our SRR to the highest setting for complete
   * 16 bit by 44KHz sample rate... */
  bitcrusher1.bits(16);
  bitcrusher1.sampleRate(MAX_FREQUENCY);
  bitcrusher2.bits(16);
  bitcrusher2.sampleRate(MAX_FREQUENCY);
  bitcrusher3.bits(16);
  bitcrusher3.sampleRate(MAX_FREQUENCY);
  bitcrusher4.bits(16);
  bitcrusher4.sampleRate(MAX_FREQUENCY);
  
/*Each mixer will control a specific channel following a bitcrusher object
 * to give volume, "level", control to the user... */
  mixer1.gain(0, 1.0);
  mixer2.gain(0, 1.0);
  mixer3.gain(0, 1.0);
  mixer4.gain(0, 1.0);

  /*Update the buttons for the debounce library... */
  gate1.update();
  gate2.update();
  gate3.update();
  gate4.update();
//  encoder0.update();
//  encoder1.update();
//  encoder2.update();
  
  delay(1000);  // Ensures all the proper setup is completed before going to the loop...
}  

void loop() {
  
  char fname[100]; //Shouldnt need more than 100 characters in a file name
  char fname1[100];
  char fname2[100];
  char fname3[100];
     
  //Serial.println("Inside Loop"); //Debugging
  //Update the buttons for every loop for the debounce library
//  encoder0.update();
//  encoder1.update();
//  encoder2.update();
  gate1.update();
  gate2.update();
  gate3.update();
  gate4.update();
  
/*This combination needs to be reworked for the encoded combination of channel selections and traversing... */  
/*ChannelSelection bits 0 and 1 and 2 are used to traverse the vector of tracks... */ 
/*  if (encoder2.read() && encoder1.read() && !encoder0.read())
  { 
      if(num == tracks.size() - 1)
      {//If num is at max value then wrap around... 
        num = 0;
      }
      else
      {
        num++;
      }
  }//if 111 then right button pressed... 
  if (encoder2.read() && encoder1.read() && encoder0.read())
  { 
      if(num == 0)
      {//If num is at 0 then wrap around to max value...  
        num = tracks.size() - 1;
      }
      else
      {
        num--;
      }
  }
*/  
  /*Code needed for encoder to assign a track to a channel... */

/*The gate buttons trigger the channels to begin playing from the SD card
which will light the appropriate LED... */
if(!gate1.read())
{   digitalWrite(5, HIGH);}
if(!gate2.read())
{   digitalWrite(4, HIGH);}
if(!gate3.read())
{   digitalWrite(8, HIGH);}
if(!gate4.read())
{   digitalWrite(24, HIGH);} 

/*Once the gates are released the lights will go out... */
if(gate1.read())
{   digitalWrite(5, LOW);}
if(gate2.read())
{   digitalWrite(4, LOW);}
if(gate3.read())
{   digitalWrite(8, LOW);}
if(gate4.read())
{   digitalWrite(24, LOW);} 

if (gate1.fallingEdge())
{
      /*Convert the string to char array to send to playSdWav()... */
      tracks[21].toCharArray(fname, sizeof(fname));
      playSdWav1.play("SINE.WAV");// fname
      delay(20); // wait for library to parse WAV info
}
if (gate2.fallingEdge())
{
      tracks[21].toCharArray(fname1, sizeof(fname1));
      playSdWav2.play("SINE.WAV"); //fname1
      delay(20); // wait for library to parse WAV info    
    
}
if (gate3.fallingEdge())
{
      tracks[2].toCharArray(fname2, sizeof(fname2));
      playSdWav3.play("SINE.WAV");
      delay(20); // wait for library to parse WAV info    
    
}
if (gate4.fallingEdge())
{
      tracks[3].toCharArray(fname3, sizeof(fname3));
      playSdWav4.play("SINE.WAV");
      delay(20); // wait for library to parse WAV info    
    
}


    /*Analog read pins for level and srr */
    int channelOneP1 = analogRead(A2);  // knob = 0 to 1023
    channelOneP1 = (channelOneP1 - 5 < 0) ? 0 : channelOneP1;
    int channelOneP2 = analogRead(A3);  //knob = 0 to 1023
    //This next statement will ensure that any value less than 5 will be seen as zero...
    channelOneP2 = (channelOneP2 - 5 < 0) ? 0 : channelOneP2; 
    int channelOneCV1 = analogRead(A6) * 10; //  0 to 1023
    int channelOneCV2 = analogRead(A7) * 10; // 0 to 1023
    int channelTwoP1 = analogRead(A10);  // knob = 0 to 1023
    channelTwoP1 = (channelTwoP1 - 5 < 0) ? 0 : channelTwoP1;
    int channelTwoP2 = analogRead(A11);  //knob = 0 to 1023
    channelTwoP2 = (channelTwoP2 - 5 < 0) ? 0 : channelTwoP2;
    int channelTwoCV1 = analogRead(A12) * 10; //  0 to 1023
    int channelTwoCV2 = analogRead(A13) * 10; // 0 to 1023
    int channelThreeP1 = analogRead(A14);  // knob = 0 to 1023
    channelThreeP1 = (channelThreeP1 - 5 < 0) ? 0 : channelThreeP1;
    int channelThreeP2 = analogRead(A15);  //knob = 0 to 1023
    channelThreeP2 = (channelThreeP2 - 5 < 0) ? 0 : channelThreeP2;
    int channelThreeCV1 = analogRead(A16) * 10; //  0 to 1023
    int channelThreeCV2 = analogRead(A17) * 10; // 0 to 1023
    int channelFourP1 = analogRead(A18);  // knob = 0 to 1023
    channelFourP1 = (channelFourP1 - 5 < 0) ? 0 : channelFourP1;
    int channelFourP2 = analogRead(A20);  //knob = 0 to 1023
    channelFourP2 = (channelFourP2 - 5 < 0) ? 0 : channelFourP2;
    float gain1, gain2, gain3, gain4, srr1, srr2, srr3, srr4;
       
//   Serial.println(channelTwoP2); //for debugging
    /*Convert the gain values back to a number between 0 and 1... 
    Determining the difference between control voltages and potentiometers*/  
    gain1 = (((float)channelOneCV1 / 1023.0) * 3) * ((float)channelOneP1  / 1023.0);
//    gain1 = 1;
//    Serial.println(gain1);
    /*Channel Two potentiometer and cv input mixing  */
//    gain2 = (((float)channelTwoCV1 / 1023.0) * 2) +  ((float)channelTwoP1   / 1023.0);
    gain2 = 1;
//  Serial.println(gain2);    
    /*Channel Three pot and cv mixing... */
//    gain3 = (((float)channelThreeCV1 / 1023.0) * 2)  + ((float)channelThreeP1   / 1023.0);
   gain3 = 1;
    
    /*Channel Four only has pots so....*/
   gain4 = 1;
//    gain4 = (float)channelFourP1  / 1023.0;
    
  
    srr1 = ((float)channelOneCV2) * ((float)channelOneP2  / 1023.0);
//    Serial.println(srr1);
       
    srr2 = ((float)channelTwoCV2) * ((float)channelTwoP2   / 1023.0);
//    Serial.println(srr2);   
    /*Channel Three SRR mixing of pot and cv */  
    srr3 = ((float)channelThreeCV2) * ((float)channelThreeP2  / 1023.0);
    
    /*Channel Four only has a pot so... */
    srr4 = (float)channelFourP2;

    /*Saved analog values for input to the bitcrusher objects... */
    int srr1Saved = srr1;
    int srr2Saved = srr2;
    int srr3Saved = srr3;
    int srr4Saved = srr4;
    
    /*Now convert the srr value to a value for the bitcrusher... */
    float bitCrush1 = MAX_FREQUENCY - ( pow((float)srr1Saved, .125) * MAX_FREQUENCY / pow(ANALOG_MAX, .125));
    float bitCrush2 = MAX_FREQUENCY - ( pow((float)srr2Saved, .125) * MAX_FREQUENCY / pow(ANALOG_MAX, .125));
    float bitCrush3 = MAX_FREQUENCY - ( pow((float)srr3Saved, .125) * MAX_FREQUENCY / pow(ANALOG_MAX, .125));
    float bitCrush4 = MAX_FREQUENCY - ( pow((float)srr4Saved, .125) * MAX_FREQUENCY / pow(ANALOG_MAX, .125));
    if(bitCrush1 < 0) {bitCrush1 = 0;}
    if(bitCrush2 < 0) {bitCrush2 = 0;}
    if(bitCrush3 < 0) {bitCrush3 = 0;}
    if(bitCrush4 < 0) {bitCrush4 = 0;}
    
//    Serial.println(bitCrush1);
    bitcrusher1.sampleRate((int)bitCrush1);
//    bitcrusher2.sampleRate((int)bitCrush2);
    bitcrusher2.sampleRate(44100);  //For debugging...
//    bitcrusher3.sampleRate((int)bitCrush3);
    bitcrusher3.sampleRate(44100);  //For debugging...
//    bitcrusher4.sampleRate((int)bitCrush4);
    bitcrusher4.sampleRate(44100);  //For debugging...
  /*The mixers for the level adjustments... */
    mixer1.gain(0, gain1); //Channel 1 audio level adjustment
    mixer3.gain(0, gain3); //Channel 3 audio level adjustment
    mixer4.gain(0, gain4); //Channel 4 audio level adjustment
    mixer2.gain(0, gain2); //Channel 2 audio level adjustment
  
}

/***********************************************************************************
 * Function: CreateTracks()
 * Inputs: function expects a File parameter
 * Output: function does not return any value
 * Description: The function takes the File parameter as a file descriptor for
 *    the SD card.  The function parses the SD card looking for .wav files to 
 *    play and adds them to the tracks Vector.
 ***********************************************************************************/

//CreateTracks function reads all the files on the SD card and 
//adds the files to a vector of tracks to play
void CreateTracks(File dir) 
{

  String temp = "_";        //temporary files on the SD have this character starting its title...
  String temp1 = ".wav";  //checking for .wav files...
  String temp2 = ".WAV";
  String tmp; //variable for storing the track title
  
  /*While loop to parse the SD card continuously... */
  while(true) {
     
     File entry =  dir.openNextFile();
     if (! entry) {
       // no more files
       //Serial.println("**nomorefiles**");
       break;
     }
     
//     Serial.print(entry.name());
     if (!entry.isDirectory()) //Check to ensure the name added is not a directory...
     {//If entry is not a directory and is a true track then add to tracks vector...
          tmp = entry.name();
          if(!tmp.startsWith(temp) & (tmp.endsWith(temp1) | tmp.endsWith(temp2)))
          {
              tracks.push_back(entry.name()); //push the track name onto the vector...
          }
          
     } 
     entry.close();
   }
}




