# README #

This repository is for the Forvious_MixerTest sketch that is the firmware for the Teensy 3.2 microcontroller project created by the 4VS team of Steven Thai, Mehran Hossaini, Gene Crocetti and Tony Townley

To install:

Visit the Teensy website @ https://www.pjrc.com/teensy/td_libs_Audio.html

Download and Install the Teensyduino installer

Navigate from your command line and 'cd' to the Arduino/library folder after installing and input:
 
'git clone https://anthony_townley@bitbucket.org/anthony_townley/forvious-firmware.git' 

This will install the Forvious sketch into your Arduino library.  Restart the IDE and the sketch will be available to use.

Navigate into the Forvious_MixerTest folder and type:
 
'git clone http://github.com/zacsketches/Arduino_Vector.git'

This will clone the vector STL into your Arduino sketch. 
A special thanks to Zac Staples for making the lightweight library available for us to use on the beer license...

Restart the IDE again and the sketch should be fully functional...